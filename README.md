# youtube-share-video
![Share Youtube Video](ScreenShot.png)

## Check MYSQL IF CHANGE DB HOST
~~~
show variables like 'char%';
+--------------------------+----------------------------+
| Variable_name            | Value                      |
+--------------------------+----------------------------+
| character_set_client     | latin1                     |
| character_set_connection | latin1                     |
| character_set_database   | latin1                     |
| character_set_filesystem | binary                     |
| character_set_results    | latin1                     |
| character_set_server     | latin1                     | => need to be change from latin1 to utf8mb4
| character_set_system     | utf8                       |
| character_sets_dir       | /usr/share/mysql/charsets/ |
+--------------------------+----------------------------+
~~~

## RUN
~~~
docker-compose up --build -d
API http://localhost:3001
Fontend http://localhost:7000
~~~

## STEP
~~~
1. http://localhost:3001/register <= register new user with required email,username, password
2. http://localhost:3001/login <= login with required username, password
3. http://localhost:3001/share <= need token and only show when login
4. http://localhost:3001 <= alway show full list video share
~~~