package testmodels

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"gopkg.in/go-playground/assert.v1"
	"log"
	"os"
	"remitano/backend/application/controllers"
	"remitano/backend/application/models"
	"testing"
)

var server = controllers.Server{}

func TestMain(m *testing.M) {
	var err error
	err = godotenv.Load("./../../test.env")
	if err != nil {
		log.Fatalf("Error get file env %v\n", err)
	}
	log.Printf("Before calling m.Run() !!!")
	ConnectDatabase()
	run := m.Run()
	//testAddUser()
	//testDuplicateAddUsers()

	log.Printf("After calling m.Run() !!!")
	os.Exit(run)
}

func ConnectDatabase()  {
	var err error
	TestConnection := os.Getenv("DB_DRIVER")
	if TestConnection == "mysql" {
		DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
			os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"),
			os.Getenv("DB_HOST"),
			os.Getenv("DB_PORT"),
			os.Getenv("DB_NAME"))
		server.DB, err = gorm.Open(TestConnection, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestConnection)
			log.Fatal("Error message :", err)
		} else {
			fmt.Printf("DB connected %s \n", TestConnection)
		}
	}
}

func changeConfigUserTable() error {
	server.DB.Exec("SET foreign_key_checks=0")
	err := server.DB.Debug().DropTableIfExists(&models.User{}).Error
	if err != nil {
		return err
	}
	server.DB.Exec("SET foreign_key_checks=1")
	err = server.DB.Debug().AutoMigrate(&models.User{}).Error
	if err != nil {
		return err
	}
	log.Printf("changeConfigUserTable OK !!!")
	return nil
}

func TestAddUser(t *testing.T) {

	_ = changeConfigUserTable()

	user := models.User{
		Username: "demo",
		Email:    "demo@gmail.com",
		Password: "demo",
	}

	err := server.DB.Create(&user).Error
	if err != nil {
		log.Fatalf("cannot add data to users table: %v", err)
	}

	log.Printf("Add Done !!!")
}

func testDuplicateAddUsers() error {
	user := models.User{
		Username: "demo",
		Email:    "demo@gmail.com",
		Password: "demo",
	}
	err := server.DB.Create(&user).Error
	if err != nil {
		log.Printf("Add Dupplicate Done")
		return nil
	}

	log.Printf("Add Dupplicate Error")
	return nil
}

func TestSaveUser(t *testing.T)  {

	user := models.User{
		Username: "xxxx",
		Password: "Password",
		Email: "xxxx@gmail.com",

	}
	user.Prepare()
	assert.Equal(t,user.ID,0)

	u,_:= user.SaveUser(server.DB)

	assert.Equal(t,u.Email, user.Email)
	assert.NotEqual(t,u.ID,0)
}
