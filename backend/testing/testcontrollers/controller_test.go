package testcontrollers
import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	_"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	_"github.com/joho/godotenv"
	"gopkg.in/go-playground/assert.v1"
	"log"
	_"log"
	"os"
	_"os"
	"remitano/backend/application/controllers"
	"remitano/backend/application/models"
	"testing"
	"net/http"
	"net/http/httptest"
	_"github.com/gorilla/mux"
)

var server = controllers.Server{}
var userInstance = models.User{}

func TestMain(m *testing.M) {
	var err error
	err = godotenv.Load("./../../test.env")
	if err != nil {
		log.Fatalf("Error get file env %v\n", err)
	}
	log.Printf("Before calling m.Run() !!!")
	ConnectDatabase()
	run := m.Run()
	//testAddUser()
	//testDuplicateAddUsers()

	log.Printf("After calling m.Run() !!!")
	os.Exit(run)
}

func ConnectDatabase()  {
	var err error
	TestConnection := os.Getenv("DB_DRIVER")
	if TestConnection == "mysql" {
		DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
			os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"),
			os.Getenv("DB_HOST"),
			os.Getenv("DB_PORT"),
			os.Getenv("DB_NAME"))
		server.DB, err = gorm.Open(TestConnection, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestConnection)
			log.Fatal("Error message :", err)
		} else {
			fmt.Printf("DB connected %s \n", TestConnection)
		}
	}
}

func TestCreateUser(t *testing.T) {
	fmt.Println("----- Begin run call test TestCreateUser ----- ")
	samples := []struct {
		inputJSON string
		statusCode   int
		errorMessage string
		username     string
		email        string
		password     string

	}{
		{
			inputJSON:    `{"username":"username11", "email": "email@gmail.com", "password": "password"}`,
			statusCode:   201,
			username:     "username",
			email:        "email@gmail.com",
			password:	"password",
			errorMessage: "",
		},
		{
			inputJSON:    `{"username":"username12", "email": "tuanna@gmail.com", "password": "tuanna"}`,
			statusCode:   201,
			username:     "tuanna",
			email:        "tuanna@gmail.com",
			password:	"tuanna",
			errorMessage: "",
		},
	}
	ConnectDatabase()
	for _, v := range samples {

		req, err := http.NewRequest("POST", "/users", bytes.NewBufferString(v.inputJSON))
		if err != nil {
			t.Errorf("this is the error: %v", err)
		}

		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(server.CreateUser)
		handler.ServeHTTP(rr, req)

		responseMap := make(map[string]interface{})
		err = json.Unmarshal([]byte(rr.Body.String()), &responseMap)
		if err != nil {
			fmt.Printf("Cannot convert to json: %v", err)
		}
		assert.Equal(t, rr.Code, v.statusCode)
		if v.statusCode == 201 {
			assert.Equal(t, responseMap["username"], v.username)
			assert.Equal(t, responseMap["email"], v.email)
		}
		if v.statusCode == 422 || v.statusCode == 500 && v.errorMessage != "" {
			assert.Equal(t, responseMap["error"], v.errorMessage)
		}
	}

}