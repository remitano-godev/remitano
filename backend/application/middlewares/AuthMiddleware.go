package middlewares

import (
	"context"
	"errors"
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"net/http"
	"os"
	"remitano/backend/application/models"
	"remitano/backend/application/responses"
	"strconv"
	"strings"
	"time"
)

type ContextKey string
const ContextUserKey ContextKey = "tokenId"

func CrossMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//Allow CORS here By * or specific origin
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		next(w,r)
	}


}

func CORS(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Access-Control-Allow-Credentials", "true")
		w.Header().Add("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		w.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")

		if r.Method == "OPTIONS" {
			http.Error(w, "No Content", http.StatusNoContent)
			return
		}

		next(w, r)
	}
}


func AuththenticationMiddleware(next http.HandlerFunc) http.HandlerFunc  {
	return func(w http.ResponseWriter, r *http.Request) {
		err:= validateToken(r)
		if err!= nil{
			responses.ERROR(w,http.StatusUnauthorized,errors.New("Unauthorized"))
			return
		}
		next(w,r)
	}
}

func GenerateToken(u *models.User) (string,error)  {
	claims:= jwt.MapClaims{}
	claims["authorized"] = true
	claims["uid"] = u.ID
	claims["username"] = u.Username
	claims["email"] = u.Email
	tokenExpire,_:= strconv.Atoi(os.Getenv("TOKEN_EXPIRE"))
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(tokenExpire)).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256,claims)
	return token.SignedString([]byte(os.Getenv("JWT_API_SECRET")))
}

func validateToken(r *http.Request) error {
	tokenString := ExtractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("JWT_API_SECRET")), nil
	})
	if err != nil {
		fmt.Println("error => ",err)
		return err
	}

	if claims,ok:= token.Claims.(jwt.MapClaims); ok && token.Valid{
		ctx := r.Context()
		req := r.WithContext(context.WithValue(ctx, ContextUserKey, claims["uid"]))
		*r = *req
	}
	return nil
}

func ExtractToken(r *http.Request) string {
	keys := r.URL.Query()
	token := keys.Get("token")
	if token != "" {
		return token
	}
	bearerToken := r.Header.Get("Authorization")
	if len(strings.Split(bearerToken, " ")) == 2 {
		return strings.Split(bearerToken, " ")[1]
	}
	return ""
}

func GetIdFromToken(r *http.Request) (int,error)  {
	var id = r.Context().Value(ContextUserKey)
	uid, err := strconv.ParseUint(fmt.Sprintf("%.0f", id), 10, 32)
	if err != nil {
		return 0,err
	}
	return int(uid),nil
}