package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"remitano/backend/application/helpers"
	"remitano/backend/application/middlewares"
	"remitano/backend/application/models"
	"remitano/backend/application/responses"
)

func (server *Server) CreateUser(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err !=nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}
	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	user.Prepare()
	err = user.Validate("create")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	userCreated, err := user.SaveUser(server.DB)
	if err != nil {
		formatError := helpers.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formatError)
		return
	}
	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, userCreated.ID))

	responses.JSON(w, http.StatusCreated, userCreated)
}

func (s *Server) Login(w http.ResponseWriter, r *http.Request)  {
	user := models.User{}
	// get data form request
	body,err:= ioutil.ReadAll(r.Body)
	if err!= nil {
		responses.ERROR(w,http.StatusUnprocessableEntity,err)
		return
	}
	err = json.Unmarshal(body,&user)
	if err!= nil {
		responses.ERROR(w,http.StatusUnprocessableEntity,err)
		return
	}
	user.Prepare()
	err = user.Validate("login")
	if err!= nil {
		responses.ERROR(w,http.StatusUnprocessableEntity,err)
		return
	}
	token,err := s.SignIn(&user,user.Password)
	if err != nil {
		responses.ERROR(w,http.StatusUnprocessableEntity,err)
		return
	}

	result:= make(map[string]string)
	result["token"] = token
	result["username"] = user.Username
	result["email"] = user.Email

	responses.JSON(w,http.StatusOK,result)
	return

}

func (s *Server) SignIn(user *models.User,password string) (string,error) {
	var err error
	err = s.DB.Model(models.User{}).Where("username = ?",user.Username).Take(&user).Error
	if err != nil {
		return "", err
	}
	err = user.VerifyPassword(password)
	if err != nil {
		return "", err
	}
	return middlewares.GenerateToken(user)
}

