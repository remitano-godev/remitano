package controllers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/rs/cors"
	"log"
	"net/http"
	"remitano/backend/application/middlewares"
	"remitano/backend/application/models"
	"remitano/backend/application/responses"
)

type Server struct {
	DB     *gorm.DB
	Router *mux.Router
}

func (server *Server) Initialize(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) {
	var err error
	sqlConnection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)
	server.DB, err = gorm.Open(Dbdriver, sqlConnection)
	if err != nil {
		fmt.Printf("Cannot connect to %s database", Dbdriver)
		log.Fatal("This is the error:", err)
	} else {
		fmt.Printf("Connected to the %s database", Dbdriver)
	}
	fmt.Println(sqlConnection)
	server.DB.Debug().AutoMigrate(&models.User{}, &models.Share{}) //database migration

	server.Router = mux.NewRouter()

	server.initializeRoutes()
}

func (server *Server) Run(addr string) {
	fmt.Println("Listening to port #",addr)
	log.Fatal(http.ListenAndServe(addr, cors.AllowAll().Handler(server.Router)))
}


func (s *Server) initializeRoutes() {
	s.Router.HandleFunc("/", middlewares.CORS(func(w http.ResponseWriter, r *http.Request) {
		type Data struct {
			Reply string
		}
		d:= Data{
			"Pong",
		}
		responses.JSON(w, http.StatusOK, d)
	}))

	s.Router.HandleFunc("/register",s.CreateUser).Methods(http.MethodPost)
	s.Router.HandleFunc("/login",middlewares.CORS(s.Login)).Methods(http.MethodPost)
	s.Router.HandleFunc("/share",middlewares.CORS(middlewares.AuththenticationMiddleware(s.Share))).Methods(http.MethodPost)
	s.Router.HandleFunc("/shares",s.GetShares).Methods(http.MethodGet)

}

