package controllers

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"remitano/backend/application/helpers"
	"remitano/backend/application/middlewares"
	"remitano/backend/application/models"
	"remitano/backend/application/responses"
)

func (s *Server) Share(w http.ResponseWriter, r *http.Request)  {
	body, err := ioutil.ReadAll(r.Body)
	if err !=nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}
	share:= models.Share{}
	err = json.Unmarshal(body, &share)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	err = share.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	uid,_:= middlewares.GetIdFromToken(r)
	share.Uid = uid
	share.Prepare()
	if len(share.Title) == 0 {
		responses.ERROR(w, http.StatusInternalServerError, errors.New("Error when call API youtube, pls check your URL"))
		return
	}
	shareCreated, err := share.SaveShare(s.DB)
	if err != nil {
		formatError := helpers.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formatError)
		return
	}
	responses.JSON(w,http.StatusOK,shareCreated)
	return
}

func (s *Server) GetShares(w http.ResponseWriter, r *http.Request)  {
	share:= models.Share{}
	shares,err:= share.FindAllShares(s.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, shares)
	return
}
