package helpers

import (
	"errors"
	"strings"
)

func FormatError(err string) error {

	if strings.Contains(err, "username") {
		return errors.New("username error")
	}

	if strings.Contains(err, "hashedPassword") {
		return errors.New("Incorrect Password")
	}
	if strings.Contains(err,"Duplicate entry") {
		return errors.New(err)
	}

	return errors.New("Incorrect Details")
}
