package models

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"html"
	"net/url"
	"os"
	"remitano/backend/application/service"
	"strings"
	"time"
)

type Share struct {
	ID	int    `gorm:"primary_key;auto_increment" json:"id"`
	VideoID	string 	`gorm:"size:100;not null;unique" json:"video_id"`
	Title	string 	`gorm:"size:255;charset:utf8mb4;not null" json:"title"`
	ChannelTitle	string 	`gorm:"size:255;not null" json:"channelTitle"`
	Description	string 	`gorm:"type:text;charset:utf8mb4;not null" json:"description"`
	ViewCount string	`json:"viewCount"`
	LikeCount string	`json:"likeCount"`
	UrlSource	string    `gorm:"size:200;not null" json:"url"`
	UrlShare	string    `gorm:"size:200;not null" json:"url_share"`
	Uid	int `sql:"type:int REFERENCES users(id)" json:"uid""`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

type Result struct {
	ID	int
	VideoID	string
	Title	string
	ChannelTitle	string
	Description	string
	ViewCount string
	LikeCount string
	UrlSource	string
	UrlShare	string
	Uid	int
	Email string
}

func (sh *Share) SaveShare(db *gorm.DB) (*Share, error) {
	var err error
	err = db.Create(&sh).Error
	if err != nil {
		return &Share{}, err
	}
	return sh, nil
}

func (sh *Share) Prepare() {
	defer func() {
		if err := recover(); err != nil {
		fmt.Println("Error when call api, pls check your url")
		}
	}()
	sh.ID = 0
	sh.UrlSource = html.EscapeString(strings.TrimSpace(sh.UrlSource))
	data:= service.Response{}
	data.IdFind = sh.VideoID
	data.GetDataFromVideoID()
	if len(data.Items) > 0 {
		sh.Title = data.Items[0].Snippet.Title
		sh.Description = data.Items[0].Snippet.Description
		sh.ChannelTitle = data.Items[0].Snippet.ChannelTitle
		sh.ViewCount = data.Items[0].Statistics.ViewCount
		sh.LikeCount = data.Items[0].Statistics.LikeCount
		sh.CreatedAt = time.Now()
		sh.UpdatedAt = time.Now()
	}
}

func (sh *Share) Validate() error {
	if sh.UrlSource == "" {
		return errors.New("Required URL")
	}else{
		url, _ := url.Parse(sh.UrlSource)
		v,err := parserURL(url)
		if err != nil {
			return err
		}
		sh.VideoID = v
		sh.UrlShare= os.Getenv("LINK_SHARE")+v
		if err !=nil {
			fmt.Println(err)
		}
	}

	return nil
}


func parserURL(u *url.URL) (string,error) {
	if u.Host != "" {
		if u.Host != os.Getenv("DOMAIN_SHARE") {
			return "",errors.New("Url InVaild, Only accept from domain "+ os.Getenv("DOMAIN_SHARE"))
		}
	}else{
		return "",errors.New("Format URL InVaild")
	}

	if u.RawQuery != "" {
		m, _ := url.ParseQuery(u.RawQuery)
		if v, found := m["v"]; found {
			return v[0],nil
		}else{
			return "",errors.New("URL Params InVaild")
		}
	}else{
		return "",errors.New("URL Params InVaild")
	}

	return "",nil
}

func (sh *Share) FindAllShares(db *gorm.DB) ([]Result, error) {
	results := []Result{}
	db.Model(&Share{}).Select("*").Joins("left join users On shares.uid = users.id").Order("shares.id desc").Scan(&results)
	return results, nil
}