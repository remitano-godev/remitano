package service

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type Response struct {
	Kind 	string	`json:"kind"`
	Etag	string `json:"etag"`
	Items	[]Item	`json:"items"`
	IdFind string	`-`
}

type Item struct {
	Kind	string	`json:"kind"`
	Etag	string `json:"etag"`
	Id 		string `json:"id"`
	Snippet Snippets `json:"snippet"`
	Statistics Statistics	`json:"statistics"`
}

type Snippets struct {
	PublishedAt string	`json:"publishedAt"`
	ChannelTitle string	`json:"channelTitle"`
	Title string	`json:"title"`
	Description string 	`json:"description"`
}

type Statistics struct {
	ViewCount string 	`json:"viewCount"`
	LikeCount string	`json:"likeCount"`
	FavoriteCount string	`json:"favoriteCount"`
	CommentCount string 	`json:"commentCount"`

}

func handlePanic()  {
	r := recover()
	if r!= nil {
		fmt.Println("Cover Panic by Recover : ",r)
	}
}

func failOnError(err error, msg string)  {
	defer handlePanic()
	if err!= nil {
		log.Panicf("%s : %s #from func FaileOnError ====== ",err)
	}
}


func (r *Response) GetDataFromVideoID() (*Response,error)  {
	urlAPI := os.Getenv("YOUTUBE_API_URL")
	apiKey := os.Getenv("YOUTUTBE_API_KEY")
	dataGet := os.Getenv("YOUTUBE_PART")
	method := "GET"
	client := &http.Client{}
	req, err := http.NewRequest(method, urlAPI, nil)

	if err != nil {
		failOnError(err,"erro if 1")
		return nil,err
	}
	q := req.URL.Query()
	q.Add("part",dataGet)
	q.Add("id", r.IdFind)
	q.Add("key",apiKey)
	req.URL.RawQuery = q.Encode()
	res, err := client.Do(req)
	if err != nil {
		failOnError(err,"erro if 2")
		return nil,err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		failOnError(err,"erro if 3")
		return nil,err
	}
	err = json.Unmarshal(body,&r)
	if err != nil {
		failOnError(err,"erro if 4")
		return nil,err
	}
	return r,nil

}