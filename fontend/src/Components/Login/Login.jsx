import {useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import {loginUser} from "../../api/apiRequest";
import {useDispatch} from "react-redux";

const Login = () => {
    const [username,setUsername] = useState(null)
    const [password,setPassword] = useState(null)
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleLogin = (e) =>{
        e.preventDefault()
        const user = {
            username: username,
            password: password
        }
        if (user.username && user.password){
            loginUser(user,dispatch,navigate);
        }
    }

    return (
        <div className="login-page">
            <div className="login-box">
                <div className="login-logo">
                    <a href="#"><b>CMS</b></a>
                </div>
                {/* /.login-logo */}
                <div className="card">
                    <div className="card-body login-card-body">
                        <p className="login-box-msg">Fill username and password to login</p>
                        <form onSubmit={handleLogin}>
                            <div className="input-group mb-3">
                                <input type="text"
                                       className="form-control" required={true}
                                       maxLength={20} minLength={5} placeholder="username"
                                       onChange={(event) =>setUsername(event.target.value) }
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password"
                                       className="form-control"
                                       required={true} maxLength={20} minLength={5} placeholder="Password"
                                       onChange={(event) =>setPassword(event.target.value) }
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-passport" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <button type="submit" className="btn btn-primary btn-block">Login</button>
                                </div>
                                {/* /.col */}
                            </div>
                        </form>
                        <p className="mb-0">
                            <a href="/register" className="text-center">Register a new membership</a>
                        </p>
                    </div>
                    {/* /.login-card-body */}
                </div>
            </div>
        </div>
    );
}

export default Login;
