import { useState } from "react";
import { logoutCall } from "./../../api/apiRequest";
import {Link, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
const NavBar = () => {
    const user = useSelector((state) => state.auth.login?.currentUser);
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const handleLogout = (e) => {
        logoutCall(dispatch,navigate)
    }

    return (
        <nav className="main-header navbar navbar-expand-md navbar-light navbar-white">
            <div className="container">
                <ul className="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
                    <li className="nav-item">
                        <Link to="/" className="nav-link">Home</Link>
                    </li>
                    {user? (
                        <>
                            <li className="nav-item">
                                <Link to="/share" className="nav-link">Share</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/" className="nav-link">Hi, <span> {user?.username}  </span></Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/login" className="nav-link" onClick={handleLogout}>Logout</Link>
                            </li>
                        </>
                        ):(
                        <>
                            <li className="nav-item">
                                <Link to="/register" className="nav-link">Register</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/login" className="nav-link">Login</Link>
                            </li>
                        </>
                        )}

                </ul>
            </div>
        </nav>
    );
};

export default NavBar;
