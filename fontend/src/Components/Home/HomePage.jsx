import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {shareGetAll} from "./../../api/apiRequest"
const HomePage = () => {
    const shareList = useSelector((state) => state.share.shareGetAll?.listShare);
    console.log("shareList=> ",shareList)
    const dispatch = useDispatch();
    useEffect(()=>{
        shareGetAll(dispatch)
    },[])

    return (
        <div className="content-wrapper" style={{minHeight: '1506px'}}>
            <div className="contain">
                <div className="container">
                    {shareList?.map((data) => {
                        return (
                                <div className="row" key={data.ID}>
                                    <div className="col-lg-7">
                                        <iframe width={700} height={315}
                                                src={data.UrlShare}
                                                title="YouTube video player"
                                                frameBorder={0}
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowFullScreen
                                        />
                                    </div>
                                    <div className="col-lg-5">
                                        <h3 className="card-title text-bold">{data.Title}</h3>
                                        <br className="line"/>
                                        <p><span className="text-bold">likeCount </span> : {data.LikeCount?data.LikeCount:0} <span className="text-bold">viewCount </span> : {data.ViewCount?data.ViewCount:0} </p>
                                        <p><span className="text-bold">Share by :</span> <span>{data.Email}</span></p>
                                        <p className="card-text">
                                            <span className="text-bold">Channel </span>: {data.ChannelTitle}
                                        </p>
                                        <p className="card-text">
                                            {data.Description.length > 250 ?
                                                `${data.Description.substring(0, 250)} .....` : data.Description
                                            }
                                        </p>
                                    </div>
                                </div>
                        );
                    })}
                </div>
            </div>
        </div>

    );
};

export default HomePage;
