import {useState} from "react";
import {registerUser} from "../../api/apiRequest";
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router-dom";
const Register = () => {
    const [username,setUsername] = useState("")
    const [password,setPassword] = useState("")
    const [email,setEmail] = useState("")
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const handleSubmit = (e) =>{
        e.preventDefault()
        const user = {
            username: username,
            password: password,
            email: email
        }
        registerUser(user,dispatch,navigate)
    }
    return (
        <div className="login-page">
            <div className="login-box">
                <div className="login-logo">
                    <a href="#"><b>Register</b> account</a>
                </div>
                {/* /.login-logo */}
                <div className="card">
                    <div className="card-body login-card-body">
                        <p className="login-box-msg">Register your account to access feature</p>
                        <form action="" method="post" onSubmit={handleSubmit}>
                            <div className="input-group mb-3">
                                <input type="email" className="form-control"
                                       required={true} placeholder="Email"
                                       onChange={(e)=> setEmail(e.target.value)}
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control"
                                       required={true} maxLength={20} minLength={5} placeholder="username"
                                       onChange={(e)=> setUsername(e.target.value)}
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password" className="form-control"
                                       required={true} maxLength={20} minLength={5} placeholder="Password"
                                       onChange={(e)=> setPassword(e.target.value)}
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-passport" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <button type="submit" className="btn btn-primary btn-block">New Account</button>
                                </div>
                                {/* /.col */}
                            </div>
                        </form>
                        <p className="mt-3 mb-1">
                            <a href="/login">Login</a>
                        </p>
                    </div>
                    {/* /.login-card-body */}
                </div>
            </div>
        </div>
    );
}

export default Register;
