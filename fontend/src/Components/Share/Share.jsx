import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import {useEffect, useState} from "react";
import {shareCreate} from "../../api/apiRequest";

const Share = () => {
    const user = useSelector((state) => state.auth.login?.currentUser);
    const [url,setUrl] = useState("")
    const dispatch = useDispatch();
    const navigate = useNavigate();
    useEffect(() =>{
        if (!user){
            navigate("/")
        }
    },[])
    const token = user?.token?user.token:""
    const handleSubmit = (e) => {
        e.preventDefault()
        if (url) {
            shareCreate({url:url},dispatch,navigate,token)
        }
    }
    return (
        <div className="login-page">
            <div className="login-box">
                <div className="login-logo">
                    <a href="#"><b>Share Video Link</b></a>
                </div>
                <div className="card">
                    <div className="card-body login-card-body">
                        <p className="login-box-msg">Fill youtube <b>URL</b> to share</p>
                        <form action="" method="post" onSubmit={handleSubmit}>
                            <div className="input-group mb-3">
                                <input type="url" className="form-control"
                                       required={true} placeholder="URL"
                                       onChange={(e)=> setUrl(e.target.value)}
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-link" />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <button type="submit" className="btn btn-primary btn-block">New Account</button>
                                </div>
                            </div>
                        </form>
                        <p className="mt-3 mb-1">
                            <a href="/login">Login</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Share;
