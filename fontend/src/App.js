import { BrowserRouter as Router, Route, Routes} from "react-router-dom";
import NavBar from "./Components/NavBar/NavBar";
import HomePage from "./Components/Home/HomePage";
import Login from "./Components/Login/Login";
import Register from "./Components/Register/Register";
import Share from "./Components/Share/Share";
import Logout from "./Components/Logout/Logout";

function App() {
  return (
          <div className="wrapper">
              <Router>
                  <NavBar />
                  <Routes>
                      <Route path="/" element={<HomePage />}/>
                      <Route path="/login" element={ <Login />}/>
                      <Route path="/register" element={<Register />}/>
                      <Route path="/share" element={<Share />}/>
                  </Routes>
              </Router>
          </div>
  );
}

export default App;
