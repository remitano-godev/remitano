import {createSlice} from "@reduxjs/toolkit";


const shareSlice = createSlice({
    name: "share",
    initialState: {
        shareGetAll: {
            listShare: null,
            isFetching : false,
            error : false
        },
        shareCreate: {
            isFetching : false,
            error : false,
            success: false
        }
    },
    reducers: {
        /* share get Reducers */
        shareGetAllStart: (state)=>{
            state.shareGetAll.isFetching = true;
        },
        shareGetAllSuccess :(state,action)=> {
            state.shareGetAll.isFetching = false;
            state.shareGetAll.listShare = action.payload
            state.shareGetAll.error = false;
        },
        shareGetAllFailed :(state,action)=> {
            state.shareGetAll.isFetching = false;
            state.shareGetAll.error = true;
        },

        /* share add Reducers */
        shareCreateStart: (state)=>{
            state.shareCreate.isFetching = true;
        },
        shareCreateSuccess :(state)=> {
            state.shareCreate.isFetching = false;
            state.shareCreate.success = true;
            state.shareCreate.error = false;
        },
        shareCreateFailed :(state,action)=> {
            state.shareCreate.isFetching = false;
            state.shareCreate.error = true;
            state.shareCreate.success = true;
        },
    }
})


export const {
    shareGetAllStart,
    shareGetAllSuccess,
    shareGetAllFailed,

    shareCreateStart,
    shareCreateSuccess,
    shareCreateFailed

} = shareSlice.actions;

export default shareSlice.reducer
