import {createSlice} from "@reduxjs/toolkit";


const authSlice = createSlice({
    name: "auth",
    initialState: {
        login: {
            currentUser: null,
            isFetching : false,
            error : false
        },
        register: {
            isFetching : false,
            error : false,
            success: false
        }
    },
    reducers: {
        /* Login Reducers */
        loginStart: (state)=>{
            state.login.isFetching = true;
        },
        loginSuccess :(state,action)=> {
            state.login.isFetching = false;
            state.login.currentUser = action.payload
            state.login.error = false;
        },
        loginFailed :(state,action)=> {
            state.login.isFetching = false;
            state.login.error = true;
        },

        /* Logout */
        logoutSuccess: (state) => {
            state.login.isFetching = false;
            state.login.currentUser = null;
            state.login.error = false;
        },
        /* Register Reducers */
        registerStart: (state)=>{
            state.register.isFetching = true;
        },
        registerSuccess :(state)=> {
            state.register.isFetching = false;
            state.register.success = true;
            state.register.error = false;
        },
        registerFailed :(state,action)=> {
            state.register.isFetching = false;
            state.register.error = true;
            state.register.success = true;
        },

    }
})


export const {
    loginStart,
    loginSuccess,
    loginFailed,
    logoutSuccess,
    registerStart,
    registerSuccess,
    registerFailed

} = authSlice.actions;

export default authSlice.reducer
