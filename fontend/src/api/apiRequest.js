import axios from "axios";
import {loginFailed, loginStart, loginSuccess, logoutSuccess} from "../redux/authSlice";
import {registerStart, registerSuccess, registerFailed} from "../redux/authSlice";
import {shareCreateFailed, shareCreateStart, shareCreateSuccess} from "../redux/shareSlice";
import {shareGetAllFailed, shareGetAllStart, shareGetAllSuccess} from "../redux/shareSlice";

import {API_LOGIN_PATH, API_REGISTER_PATH, API_SHARE_PATH,API_SHARES_PATH} from "../config/constant";

export const loginUser = async (user,dispatch,navigate) => {
    dispatch(loginStart())
    try {
        const res = await axios.post(API_LOGIN_PATH,user);
        dispatch(loginSuccess(res.data))
        navigate("/")
    }catch (e) {
        let errMessage = e.message + ": " + e.response.data.error
        alert(errMessage)
        dispatch(loginFailed())
    }
}


export const registerUser = async (user,dispatch,navigate) => {
    dispatch(registerStart())
    try {
        const res = await axios.post(API_REGISTER_PATH,user);
        dispatch(registerSuccess(res.data))
        navigate("/login")
    }catch (e) {
        let errMessage = e.message + ": " + e.response.data.error
        alert(errMessage)
        dispatch(registerFailed())
        console.log(e)
    }
}

export const shareGetAll = async (dispatch) => {
    dispatch(shareGetAllStart())
    try {
        const res = await axios.get(API_SHARES_PATH);
        dispatch(shareGetAllSuccess(res.data))
    }catch (e) {
        let errMessage = e.message + ": " + e.response.data.error
        alert(errMessage)
        dispatch(shareGetAllFailed())
    }
}

export const shareCreate = async (url,dispatch,navigate,accessToken) => {
    dispatch(shareCreateStart())
    try {
        const res = await axios.post(API_SHARE_PATH,url,{
            headers: {Authorization: `Bearer ${accessToken}`}
        });
        dispatch(shareCreateSuccess(res.data))
        navigate("/")
    }catch (e) {
        let errMessage = "Please check your url, it was exit or invalid"
        alert(errMessage)
        console.log(e)
        dispatch(shareCreateFailed())
    }
}

export const logoutCall = async (dispatch,navigate) => {
    dispatch(logoutSuccess());
    navigate("/login");
}
